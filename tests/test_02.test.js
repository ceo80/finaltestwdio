import { expect } from 'chai'
import HomePage from './pages/home.page'
import LoginPage from './pages/login.page'
import AccountPage from './pages/account.page'
import RegisterPage from './pages/register.page'

import {getRandomString} from '../utils/helperMethods.util'

describe("Register Page", function(){
    it('should registry correctly', function(){
        HomePage.openPage("/");
        addStep(`Click in button Sing in`);
        HomePage.clickSigInButton();
        expect(LoginPage.getPageTitle()).to.equal('AUTHENTICATION');
        addStep(`Insert new email and press button 'Create Account'`);
        LoginPage.startNewRegister(getRandomString() + '@nuevo.com');
        addStep(`Insert all personal information`);
        RegisterPage.completePersonalInformation(1, 'Jhon','Foo','123456','12',"4","1985");
        addStep(`Insert all address information`);
        RegisterPage.completeAddressInformation('Jhon','Foo', 'street1', 'street2', 'montreal', '23', '11600', '09844221177', 'my address');
        expect(AccountPage.getPageTitle()).to.equal('MY ACCOUNT');
    });
})