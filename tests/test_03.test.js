import { expect } from 'chai'

import HomePage from './pages/home.page'
import searchPage from './pages/search.page'
import data from '../data/articles.data'

describe("Bar Search", () => {
    data.forEach((article) => {
        it(`should searching correctly ${article.qty} ${article.name}`, () => {
            HomePage.openPage("/");
            addStep(`Type a article name in the search bar`);
            HomePage.searchArticle(`${article.name}`)
            expect(searchPage.getPageTitle()).to.contains(`${article.name}`.toLocaleUpperCase());
            expect(searchPage.countArticlesFound()).to.equal(article.qty)
        });
    })
})