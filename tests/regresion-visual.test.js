import HomePage from './pages/home.page'
/**
 * Se valida el banner que esta arriba del todo en el sitio
 */

describe.skip('Regresion Visual Banner ecommers', () => {
    it('', () => {
        HomePage.openPage("http://automationpractice.com/index.php");
    
        $(".banner").waitForDisplayed();
    
        expect(browser.checkElement($(".banner"), "wdio-headerContainer", {
                /* opciones de configuración para el elemento */
            }),
                "Error: the current banner doesn't match the original banner"
        ).equal(0);
    });
 })

