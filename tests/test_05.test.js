import { expect } from 'chai'
import ArticlePage from './pages/article.page'
import HomePage from './pages/home.page'
import CartPage from './pages/cart.page'
import LoginPage from './pages/login.page'


describe.only("Checkout", function(){
    it('should can complete the checkout proccess', function(){
        let search_query = 'Blouse'
        HomePage.openPage(`/index.php?id_product=2&controller=product&search_query=${search_query}&results=1`);
        ArticlePage.addToCart();
        ArticlePage.clickButtonProceedToCheckout();
        expect(CartPage.getPageTitle()).to.equal("SHOPPING-CART SUMMARY\nYour shopping cart contains: 1 Product");
        expect(CartPage.getItemName()).to.equal(`${search_query}`);
        CartPage.clickInButtonProceed();
        addStep(`Step 02. Sign in`);
        expect(CartPage.getPageTitle()).to.equal('AUTHENTICATION');
        addStep(`Step 02. Sign in - Insert the user email and pass and to press in 'Sing In' button`)
        LoginPage.login("rnzparente@gmail.com", "123456")
        addStep(`Step 03. Address`);
        expect(CartPage.getDeliveryAddressText()).to.equal("YOUR DELIVERY ADDRESS");
        expect(CartPage.getBillingAddressText()).to.equal("YOUR BILLING ADDRESS");
        CartPage.clickInButtonProceed();
        addStep(`Step 04. Shipping`);
        CartPage.acceptTermsAndConditions();
        CartPage.clickInButtonProceed();
        addStep(`Step 05. Payment. Pay by bank wire`);
        CartPage.clickPayByBank();
        CartPage.clickConfirmOrder();
        expect(CartPage.getPageTitle()).to.equal("ORDER CONFIRMATION");
    });
})