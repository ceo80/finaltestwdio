import { expect } from 'chai'
import ArticlePage from './pages/article.page'
import HomePage from './pages/home.page'
import LoginPage from './pages/login.page'
import AccountPage from './pages/account.page'

describe("Login Page", () => {
    it('should sign in correctly', () => {
        HomePage.openPage("/");
        addStep(`Click in button Sing in`)
        HomePage.clickSigInButton();
        expect(LoginPage.getPageTitle()).to.equal('AUTHENTICATION');
        addStep(`Insert the user email and pass and to press in 'Sing In' button`)
        LoginPage.login("rnzparente@gmail.com", "123456")
        expect(AccountPage.getPageTitle()).to.equal('MY ACCOUNT');
    });
})