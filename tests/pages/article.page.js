import BasePage from './base.page';

class CartPage extends BasePage {

    //Elementos Web
    get buttonAddToCart(){ return $('#add_to_cart')}
    get buttonGoToCheckout(){return $('//span[normalize-space()="Proceed to checkout"]')}

    //-------------------------------------------------------------------------------------------------------//
  
    /**
     * Click on button Add To Cart
     */
    addToCart() {
        addStep(`Add item to cart`);
        super.clickElement(this.buttonAddToCart);
    }

    /**
     * Click in button Proceed to checkout
     */
    clickButtonProceedToCheckout(){
        addStep(`Proceed to checkout`);
        super.clickElement(this.buttonGoToCheckout);
    }
 }

export default new CartPage();
