

import BasePage from './base.page' 

class LoginPage extends BasePage{
    //WebElements
    get inputEmail(){ return $('#email') }
    get inputPass(){return $('#passwd')}
    get buttonSubmitLogin(){return $('#SubmitLogin')}
    get inputCreateEmail(){return $('#email_create')}
    get buttonCreateAccount(){return $('#SubmitCreate')}

    get headerPageTitle(){return $('//*[@id="center_column"]/h1')}

    /**
     * Login user into the site
     * @param {String} email 
     * @param {String} pass 
     */
    login(email, pass){
        super.clearAndSendKeys(this.inputEmail, email);
        super.clearAndSendKeys(this.inputPass, pass);
        super.clickElement(this.buttonSubmitLogin);
    }

    /**
     * return the header text to Login Page
     * @returns String page title
     */
    getPageTitle(){
        return super.getTextElement(this.headerPageTitle);
    }

    /**
     * 
     * @param {String} email 
     */
    startNewRegister(email){
        super.clearAndSendKeys(this.inputCreateEmail, email);
        super.clickElement(this.buttonCreateAccount);
    }
}

export default new LoginPage();