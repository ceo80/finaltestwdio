import BasePage from './base.page' 

class LoginPage extends BasePage{
    //WebElements
    get headerPageTitle(){return $('#center_column h1')}

    get radioGenderMale(){return $('#id_gender1, #uniform-id_gender1')}
    get radioGenderFemale(){return $('#id_gender2, #uniform-id_gender2')}
    get inputFirstName(){return $('#customer_firstname')}
    get inputLastName(){return $('#customer_lastname')}
    get inputEmail(){return $('#email')}
    get inputPassword(){return $('#passwd')}
    get dropDownListDays(){return $('#days')}
    get dropDownListMonths(){return $('#months')}
    get dropDownListYears(){return $('#years')}

    get inputAddressFirstName(){return $('#firstname')}
    get inputAddressLastName(){return $('#lastname')}
    get inputAddress1(){return $('#address1')}
    get inputAddress2(){return $('#address2')}
    get inputAddressCity(){return $('#city')}
    get dropDownListState(){return $('#id_state')}
    get inputAddressZip(){return $('#postcode')}
    get dropDownListCountry(){return $('#id_country')}
    get inputAddressPhoneMobile(){return $('#phone_mobile')}
    get inputAddressAlias(){return $('#alias')}
    get buttonSubmitAccount(){return $('#submitAccount')}

    /**
     * return the header text to Login Page
     * @returns String page title
     */
    getPageTitle(){
        return super.getTextElement(this.headerPageTitle);
    }

    /**
     * Insert personal information
     * @param {String} name 
     */
    completePersonalInformation(value_gender, name, last, pass, day, month, year){
        let gender = (value_gender == 2)?this.radioGenderFemale:this.radioGenderMale;
        super.clickElement(gender);
        super.clearAndSendKeys(this.inputFirstName, name);
        super.clearAndSendKeys(this.inputLastName, last);
        super.clearAndSendKeys(this.inputPassword, pass);

        super.selectedValueDropDownlistByValue(this.dropDownListDays, day);
        super.selectedValueDropDownlistByValue(this.dropDownListMonths, month);
        super.selectedValueDropDownlistByValue(this.dropDownListYears, year);
    }

    completeAddressInformation(name, lastname, address1, address2, city, state, zip, phone, alias){
        super.clearAndSendKeys(this.inputAddressFirstName, name);
        super.clearAndSendKeys(this.inputAddressLastName, lastname);
        super.clearAndSendKeys(this.inputAddress1, address1);
        super.clearAndSendKeys(this.inputAddress2, address2);
        super.clearAndSendKeys(this.inputAddressCity, city);
        super.selectedValueDropDownlistByValue(this.dropDownListState, state);
        super.clearAndSendKeys(this.inputAddressZip, zip);
        this.clearAndSendKeys(this.inputAddressPhoneMobile, phone);
        this.clearAndSendKeys(this.inputAddressAlias, alias);
        this.clickElement(this.buttonSubmitAccount);
    }

}

export default new LoginPage();