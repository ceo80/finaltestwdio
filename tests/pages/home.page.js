import BasePage from './base.page';

class HomePage extends BasePage {

    //WebElements
   get searchBar(){ return $('#search_query_top')}
   get buttonSingIn(){return $('=Sign in')}
   get headerPageTitle(){return $('//*[@id="center_column"]/h1')}
   //-------------------------------------------------------------------------------------------------------//

   /**
    * Escribe el artículo en el campo de búsqueda y presiona Enter
    * @param {String} articulo que se buscará
    */
   searchArticle(articleName) {
       super.clearAndSendKeys(this.searchBar, articleName);
       this.searchBar.keys('Enter');
   }

   /**
    * Obtener texto de la barra de búsqueda
    */
   /*obtenerTextoBusqueda() {
    addStep('Obtener texto de la barra de búsqueda')
       return this.barraDeBusqueda.getValue();
   }*/

   /**
    * Click in the button Sing In
    * */ 
   clickSigInButton(){
        super.clickElement(this.buttonSingIn);
   }    

    /**
     * return the header text to Login Page
     * @returns String page title
     */
     getPageTitle(){
        console.log(this.headerPageTitle.getText());
        return super.getTextElement(this.headerPageTitle);
    }
}

export default new HomePage();