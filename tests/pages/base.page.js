const PAGE_TIMEOUT = 30000
export default class BasePage {
    
    /*
    * Open Page
    * @param {String} path
    */
   openPage(route) {
        browser.url(`${route}`)
   }

   /**
    * Wait for clickable element and click this
    * @param {Object} element to click
    */
   clickElement(element){
        element.waitForClickable({ timeout: PAGE_TIMEOUT });
        element.click();
   }


   /**
     * Method to send text to an element
     * @param {Object} element that will receive the text
     * @param {String} text to send 
     */
    clearAndSendKeys(element, text){
        element.waitForClickable({ timeout: PAGE_TIMEOUT });
        //element.waitForDisplayed();
        element.clearValue();
        element.click();
        element.keys(text);
    }

    /**
     * Select item in a Dropdownlist by Index 
     * @param {Object} element dropdownlist
     * @param {String} index of the element to select 
     */
     selectedValueDropDownlistByIndex(element, index){
        element.waitForClickable();
        element.selectByIndex(index);
    }

    /**
     * Select item in a Dropdownlist by Value
     * @param {Object} element dropdownlist
     * @param {String} value of the element to select 
     */
     selectedValueDropDownlistByValue(element, value){
        element.selectByAttribute("value", value);
    }

    /**
     * Return a text of an element
     */
    getTextElement(element){
        element.waitForClickable({ timeout: PAGE_TIMEOUT });
        return element.getText();
    }

}