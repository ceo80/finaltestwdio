
import BasePage from './base.page';

class CartPage extends BasePage {

    //Elementos Web
    get headerPageTitle(){return $('//*[@id="center_column"]/h1')}
    get nameArticle(){ return $('.cart_description .product-name a')};
    get buttonProceedToCheckout(){return $('//a[@class="button btn btn-default standard-checkout button-medium"]//span[contains(text(),"Proceed to checkout")] | //*[@name="processAddress"] | //*[@name="processCarrier"]')}
    get headerDeliveryAddress(){return $('#address_delivery li:first-child')}
    get headerBillingAddress(){return $('#address_invoice li:first-child')}
    get checkTermsAnsConditions(){return $('#cvg, #uniform-cgv')}
    get linkPayByBank(){return $('#HOOK_PAYMENT div div p a')}
    get buttonConfirmOrder(){return $('#cart_navigation button span')}
    //-------------------------------------------------------------------------------------------------------//
  
   /**
     * return the header text to Login Page
     * @returns String page title
     */
    getPageTitle() {
        console.log(this.headerPageTitle.getText());
        return super.getTextElement(this.headerPageTitle);
    }

    getDeliveryAddressText(){
        return super.getTextElement(this.headerDeliveryAddress);
    }

    getBillingAddressText(){
        return super.getTextElement(this.headerBillingAddress);
    }

    getItemName(){
        return super.getTextElement(this.nameArticle);
    }

    clickInButtonProceed(){
        super.clickElement(this.buttonProceedToCheckout);
    }

    acceptTermsAndConditions(){
        super.clickElement(this.checkTermsAnsConditions);
    }

    clickPayByBank(){
        super.clickElement(this.linkPayByBank);
    }

    clickConfirmOrder(){
        super.clickElement(this.buttonConfirmOrder);
    }
 }

export default new CartPage();
