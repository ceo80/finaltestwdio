import BasePage from './base.page';

class BusquedaPage extends BasePage {

    //Elementos Web
    get headerPageTitle(){ return $('//h1[contains(@class,"product-listing")]') }
    get articleBlouseElement(){return $('//a[@title="Blouse"][normalize-space()="Blouse"]')}
    get articlesElements(){return $$('.product-container div:nth-of-type(2) h5 a')}

    //-------------------------------------------------------------------------------------------------------//
  
    /**
     * Click en el resultado de la búsqueda
     */
    ingresarAlResultado() {
        super.clickearElemento(this.articleBlouseElement);
    }

    /**
     * Obtener texto del resultado de la búsqueda
     */
    getBlouseName() {
        return this.articleBlouseElement.getText();
    }

     /**
     * return the header text to Login Page
     * @returns String page title
     */
      getPageTitle(){
        return super.getTextElement(this.headerPageTitle);
    }


    countArticlesFound() {
        return (this.articlesElements).length;
    }

 }

export default new BusquedaPage();
