
import BasePage from './base.page' 

class LoginPage extends BasePage{
    //WebElements
    get headerPageTitle(){return $('#center_column h1')}

    /**
     * return the header text to Login Page
     * @returns String page title
     */
    getPageTitle(){
        return super.getTextElement(this.headerPageTitle);
    }
}

export default new LoginPage();