import { expect } from 'chai'
import ArticlePage from './pages/article.page'
import HomePage from './pages/home.page'
import CartPage from './pages/cart.page'


describe("Add to cart", function(){
    it('should can add to cart correctly', function(){
        let search_query = 'Blouse'
        HomePage.openPage(`/index.php?id_product=2&controller=product&search_query=${search_query}&results=1`);

        addStep(`Add item to cart`);
        ArticlePage.addToCart();
        addStep(`Proceed to checkout`);
        ArticlePage.clickButtonProceedToCheckout();
        expect(CartPage.getPageTitle()).to.equal("SHOPPING-CART SUMMARY\nYour shopping cart contains: 1 Product");
        expect(CartPage.getItemName()).to.equal(`${search_query}`);
    });

})